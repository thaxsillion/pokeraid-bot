from datetime import datetime

from models.host import Host


class RaidRoom:
    room_id = 0
    remaining_invites = 0
    host: Host = None
    create_date: datetime = None
    name = ''
    gym_name = ''
    tier = 0

    def __init__(self,
                 room_id: int,
                 name: str,
                 gym_name: str,
                 tier: int,
                 remaining_invites: int,
                 host: Host,
                 create_date: str):
        self.room_id = room_id
        self.name = name
        self.gym_name = gym_name
        self.tier = tier
        self.remaining_invites = remaining_invites
        self.host = host
        self.create_date = datetime.strptime(create_date[:-4], '%a, %d %b %Y %H:%M:%S')

    @staticmethod
    def from_json(json: dict):
        host_rating = None

        if 'host_rating' in json:
            host_rating = json['host_rating']

        return RaidRoom(room_id=json['room_id'],
                        name=json['room_name'],
                        gym_name=json['gym_id'],
                        host=Host(rating=host_rating, state=json['host_state']),
                        remaining_invites=json['remaining_invite'],
                        create_date=json['create_date'],
                        tier=json['tier'])

