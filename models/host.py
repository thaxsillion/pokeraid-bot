from typing import Optional


class Host:
    state = ''
    rating: Optional[float]

    def __init__(self, state: str, rating: Optional[float]):
        self.state = state
        self.rating = rating
