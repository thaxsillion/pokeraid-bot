import requests
from typing import List
from models.raid_room import RaidRoom


class PokeRaidClient:
    cookie = ''
    pokeraid_version = ''
    headers = {}
    registration_token = ''

    def __init__(self, session: str, cfduid: str):
        self.pokeraid_version = '35'
        self.cookie = f'session={session}; __cfduid={cfduid}'
        self.headers = {
            'Accept': '*/*',
            'User-Agent': f'PokeRaid/{self.pokeraid_version} CFNetwork/1220.1 Darwin/20.3.0',
            'Accept-Language': 'en-us',
            'Cookie': self.cookie
        }
        self.registration_token = 'duItwNGcmE2cv8r-tVFt_z:APA91bGOEhVjXxmKKao66HWvHXpHzeHEmM' \
                                  '-5bpvc6FUoRu7WPqLsvb36ORTM6AodHf8faBtOavSBq-3flOJspBI4GS2' \
                                  'gPLDTiiSB0WjmIr3dKvYNU8wBRngw5oR-JDNhT7ziUk-wYEyb'

    def get_raid_rooms(self, pokedex: int) -> List[RaidRoom]:
        url = f'https://poketrade.me/api/raid_room/search' \
              f'?version={self.pokeraid_version}' \
              f'&tier=' \
              f'&app_name=pokeraid' \
              f'&country=PL' \
              f'&platform=ios' \
              f'&language=en' \
              f'&dex={str(pokedex)}' \
              f'&only_active=true' \
              f'&only_empty=false' \
              f'&registration_token={self.registration_token}'

        response = requests.get(url, headers=self.headers)

        if response.status_code > 399:
            print("[SERVER ERROR] STATUS CODE %d" % response.status_code)
            return []

        raid_rooms = response.json()['raid_rooms']
        return list(map(lambda j: RaidRoom.from_json(j), raid_rooms))

    def join_raid_room(self, room_id: int) -> dict:
        url = f'https://poketrade.me/api/raid_room/join/{str(room_id)}' \
              f'?version={self.pokeraid_version}' \
              f'&app_name=pokeraid' \
              f'&country=PL' \
              f'&platform=ios' \
              f'&language=en' \
              f'&registration_token={self.registration_token}'

        s = requests.post(url, data='{}', headers=self.headers)

        return s.json()
