import json
from typing import List


class Pokemon:
    name = ''
    types = []
    pokedex = 0

    def __init__(self, name: str, types: List[str], pokedex: int):
        self.name = name
        self.types = types
        self.pokedex = pokedex


class Pokedex:
    pokemon = {}

    def __init__(self):
        self.pokemon = {}
        self.load()

    def load(self):
        with open('pokedex.json') as json_file:
            pokemon_json = json.load(json_file)
            for entry in pokemon_json:
                pokemon = Pokemon(name=entry['name']['english'], types=entry['type'], pokedex=entry['id'])
                self.pokemon[pokemon.name] = pokemon

    def get_by_name(self, pokemon_name) -> Pokemon:
        return self.pokemon[pokemon_name]
