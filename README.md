# PokeRaid Bot

This is an auto-join pokeraid bot. It supports OSX notifications.

### Configuration:

Make sure to configure your cookie in `main.py`.
Your personal cookie can be intercepted using a proxy like Charles Proxy.

### Usage

Run using the desired pokemon name, e.g. `./main.py Tornadus`
