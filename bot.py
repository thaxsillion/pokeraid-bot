import time
from datetime import datetime
from typing import List

# from pync import Notifier
from pokedex import Pokedex
from pokeraid_client import PokeRaidClient, RaidRoom


class Bot:
    client: PokeRaidClient = None
    pokedex: Pokedex = None

    def __init__(self, session: str, cfduid: str):
        self.client = PokeRaidClient(session=session, cfduid=cfduid)
        self.pokedex = Pokedex()

    def join_raid(self, pokemon_name: str):
        pokedex_number = int(self.pokedex.get_by_name(pokemon_name).pokedex)
        found_raid_room = False

        while not found_raid_room:
            raid_rooms = self.get_active_raid_rooms(
                pokedex_number=pokedex_number
            )

            if len(raid_rooms) > 0:
                for raid_room in raid_rooms:
                    try:
                        found_raid_room = self.join_raid_room(raid_room)

                        if found_raid_room:
                            self.notify_found_raid_room(raid_room)
                            break
                    except Exception as error:
                        print(error)
                        pass

                    time.sleep(0.5)
            else:
                pokemon = self.pokedex.get_by_name(pokemon_name)
                print(
                    f'No matching {pokemon.name} [dex={pokedex_number:d}] raids.'
                )
                time.sleep(1)

    def join_raid_room(self, raid_room: RaidRoom) -> bool:
        raid_json = self.client.join_raid_room(room_id=raid_room.room_id)
        if raid_json['success']:
            return True
        else:
            return False

    def get_active_raid_rooms(self, pokedex_number: int) -> List[RaidRoom]:
        raid_rooms = self.client.get_raid_rooms(pokedex=pokedex_number)

        active_raid_rooms = list(filter(self.filter_available_rooms, raid_rooms))
        active_raid_rooms = list(filter(self.filter_old_rooms, active_raid_rooms))
        active_raid_rooms = list(filter(self.filter_bad_reputation, active_raid_rooms))
        return active_raid_rooms

    def filter_available_rooms(self, room: RaidRoom) -> bool:
        return int(room.remaining_invites) > 0 \
               and room.host.state != 'raid_starting'

    def filter_bad_reputation(self, room: RaidRoom) -> bool:
        if room.host.rating is None:
            return True

        return float(room.host.rating) >= 3.75

    def filter_old_rooms(self, room: RaidRoom) -> bool:
        current_timestamp = time.mktime(datetime.utcnow().timetuple())
        room_creation_timestamp = time.mktime(room.create_date.timetuple())

        minute_difference = int(
            int(current_timestamp - room_creation_timestamp) / 60
        )
        return minute_difference < 2

    def notify_found_raid_room(self, raid_room: RaidRoom):
        message = 'Joined %s (Tier %d) raid at gym %s.' % (
            raid_room.name,
            int(raid_room.tier),
            raid_room.gym_name
        )

        print(message)
        # Notifier.notify(message, title='Found a raid!')
