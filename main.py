import sys
from bot import Bot

bot = Bot(session='<session>', cfduid='<cfduid>')

if len(sys.argv) < 2:
    print('Enter pokemon name')
    exit(0)

pokemon = sys.argv[1]

bot.join_raid(pokemon_name=pokemon)
